<?php

  // -----------------------------------------------------------
  // exo 3
  // -----------------------------------------------------------
  // copier/coller le code de votre classe de l'exo précédent
  // 
  // puis :
  //
  // dans le constructeur :
  // donner des valeurs aux propriétés suivantes :
  // - light (bool) : true
  // - weight (int) : une valeur aléatoire comprise entre 1 et 10
  // - happiness (int) : 50
  //
  // dans la classe :
  // créer les getters getLight, getWeight et getHappiness
  // qui retournent les propriétés du même nom
  //
  // astuce : 
  // pour la valeur aléatoire, on peut passer par random_int()
  // https://www.php.net/manual/fr/function.random-int.php

  // 🐭 TAPE TON CODE ICI 🐱

  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // ne pas toucher aux lignes ci-dessous
  // qui servent à vérifier votre code ;)
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  if ( !method_exists("Tamagoshi", "getName") OR !method_exists("Tamagoshi", "getLight") OR !method_exists("Tamagoshi", "getWeight") OR !method_exists("Tamagoshi", "getHappiness") ) exit ("🔴 Erreur : il manque au moins une méthode"); $o = new Tamagoshi("RZA"); if ( strlen( $o->getName()) === 3 AND is_bool( $o->getLight() ) AND is_int( $o->getWeight() ) AND is_int( $o->getHappiness() ) ) { exit("✅ EXO validé, tu peux passer au suivant"); } else { exit("🔴 Erreur : le constructeur doit donner des valeurs aux propriétés name, light, weight et happiness"); }