<?php

  // -----------------------------------------------------------
  // exo 1
  // -----------------------------------------------------------
  // créer une classe Tamagoshi ayant les propriétés suivantes :
  // name, feed, weight, light, happiness
  //
  // ces propriétés doivent avoir la visibilité "private"

  // 🐭 TAPE TON CODE ICI 🐱

  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // ne pas toucher aux lignes ci-dessous
  // qui servent à vérifier votre code ;)
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  if (class_exists("Tamagoshi")) { if ( property_exists("Tamagoshi", "name") AND property_exists("Tamagoshi", "feed") AND property_exists("Tamagoshi", "weight") AND property_exists("Tamagoshi", "light") AND property_exists("Tamagoshi", "happiness") ) { exit("✅ EXO validé, tu peux passer au suivant"); } else { exit("🔴 Erreur : il faut déclarer toutes les propriétés demandées"); } } else { exit("🔴 Erreur : il faut créer la classe demandée"); }