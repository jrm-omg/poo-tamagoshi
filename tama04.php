<?php

  // -----------------------------------------------------------
  // exo 4
  // -----------------------------------------------------------
  // copier/coller le code de votre classe de l'exo précédent
  // 
  // puis :
  //
  // - créer une méthode switchLight qui :
  // si la propriété light vaut true, passe light à false
  // si la propriété light vaut false, passe light à true
  // - créer une méthode game qui ;
  // incrémente la propriété happiness de 10
  // décrémente la propriété weight de 1
  // - créer une méthode feed qui incrémente la propriété
  // weight de 1

  // 🐭 TAPE TON CODE ICI 🐱

  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // ne pas toucher aux lignes ci-dessous
  // qui servent à vérifier votre code ;)
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  if ( !method_exists("Tamagoshi", "switchLight") OR !method_exists("Tamagoshi", "game") OR !method_exists("Tamagoshi", "feed") ) exit ("🔴 Erreur : il manque au moins une méthode"); $o = new Tamagoshi("Ryu"); $o->switchLight(); if($o->getLight() !== false) exit("🔴 Erreur : la méthode switchLight ne fonctionne pas comme elle le devrait"); $h=$o->getHappiness(); $w=$o->getWeight(); $o->game(); if($o->getHappiness()-10 !== $h) exit("🔴 Erreur : la méthode game ne fonctionne pas comme elle le devrait"); if($o->getWeight()+1 !== $w) exit("🔴 Erreur : la méthode game ne fonctionne pas comme elle le devrait"); $w=$o->getWeight(); $o->feed(); if($o->getWeight()-1 !== $w) exit("🔴 Erreur : la méthode feed ne fonctionne pas comme elle le devrait"); exit("✅ EXO validé, et c'est le dernier de la série, alors bravo 😽");