<?php

  // -----------------------------------------------------------
  // exo 2
  // -----------------------------------------------------------
  // copier/coller le code de votre classe de l'exo précédent
  // 
  // puis :
  // 
  // - créer un constructeur qui permet de donner un nom (name)
  // au Tamagoshi
  // - créer une méthode getName qui retourne la propriété
  // du même nom (name)

  // 🐭 TAPE TON CODE ICI 🐱

  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // ne pas toucher aux lignes ci-dessous
  // qui servent à vérifier votre code ;)
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  // -----------------------------------------------------------
  if (!method_exists("Tamagoshi", "getName")) exit("🔴 Erreur : il manque le getter getName"); if (!method_exists("Tamagoshi", "__construct")) exit("🔴 Erreur : il manque le constructeur"); $o = new Tamagoshi("Redman"); if ($o->getName() !== "Redman") exit("🔴 Erreur : le getter getName ne retourne pas la bonne information"); exit("✅ EXO validé, tu peux passer au suivant");