# POO-tamagoshi

Ch'tite révision PHP orientée POO :
- création d'une classe
- déclaration de propriétés
- visibilité
- constructeur
- paramètre
- getter, setter

## Format utilisé
- les exos se suivent
- les énoncés sont inclus dans les exos
- les exos possèdent une vérification automatique

Et si à moment donné vous êtes totalement perdu, désarmé et sans espoir : il existe une branche de [correction](https://codeberg.org/jrm-omg/poo-tamagoshi/src/branch/correction) sur ce repo :)